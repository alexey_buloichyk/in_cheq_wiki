﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ConfluenceWiki
{
    internal static class XmlDocumentsParser
    {

        public static List<XElement> GetConrollerElements(string webApiXmlPath)
        {
            var root = ParseXmlFile(webApiXmlPath);
            var members = GetMembers(root);
            var controllerElements = members.Where(ControllerClassesPredicate).ToList();
            return controllerElements;
        }

        private static bool ControllerClassesPredicate(XElement element)
        {
            var controllerNamespace = ":InCheq.WebAPI.Controllers.";
            var nameAttribute = element.Attributes("name").FirstOrDefault();
            return nameAttribute != null && nameAttribute.Value.StartsWith("M" + controllerNamespace);
        }

        public static Dictionary<XElement, List<XElement>> GetModelElements(string libXmlPath)
        {
            var root = ParseXmlFile(libXmlPath);
            var members = GetMembers(root);
            var modelElementsWithProperties = new Dictionary<XElement, List<XElement>>();
            var modelElements = members.Where(ModelClassesPredicate);

            foreach (var modelElement in modelElements)
            {
                var properties = GetModelProperties(members, modelElement);
                modelElementsWithProperties.Add(modelElement, properties);
            }

            return modelElementsWithProperties;
        }

        private static IEnumerable<XElement> GetMembers(XElement root)
        {
            return root.Elements().Where(e => e.Name == "members").FirstOrDefault().Elements();
        }

        private static List<XElement> GetModelProperties(IEnumerable<XElement> members, XElement modelElement)
        {
            var name = modelElement.Attributes("name").FirstOrDefault().Value;
            var classProperties = "P" + name.Substring(1);
            var properties = members.Where(
                    e =>
                    {
                        var nameAttribute = e.Attributes("name").FirstOrDefault();
                        return nameAttribute != null && nameAttribute.Value.StartsWith(classProperties);
                    }
                )
                .ToList();
            return properties;
        }

        private static bool ModelClassesPredicate(XElement element)
        {
            var modelNamespace = ":InCheq.Lib.Services.Messaging.Requests.";
            var nameAttribute = element.Attributes("name").FirstOrDefault();
            var type = "T" + modelNamespace;
            var abstractType = "T" + modelNamespace + "Abstracts";
            return nameAttribute != null && nameAttribute.Value.StartsWith(type)
                && !nameAttribute.Value.StartsWith(abstractType);
        }

        private static XElement ParseXmlFile(string path)
        {
            using (var reader = XmlReader.Create(path))
            {
                while (reader.NodeType != XmlNodeType.Element)
                {
                    reader.Read();
                }
                var element = XElement.Load(reader);
                return element;
            }
        }

    }
}
