﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using System.Globalization;
using Dapplo.Confluence;
using Dapplo.Confluence.Query;
using System.Linq;
using System.Text;
using ConfluenceWiki.Models.Confluence;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using ConfluenceWiki.Models.WikiModels;

namespace ConfluenceWiki
{
    class Program
    {
        private static readonly HttpClient client = new HttpClient();
        private static readonly Encoding encoding = Encoding.UTF8;
        private const string spaceKey = "cu610";
        private const string webApiXmlPath = @"C:\projects\InCheqBitBucket\src\InCheq.WebAPI\bin\InCheq.WebAPI.xml";
        private const string libXmlPath = @"C:\projects\InCheqBitBucket\src\InCheq.Lib\bin\Debug\InCheq.Lib.xml";

        static void Main(string[] args)
        {
            InitClient();

            var controllerElements = XmlDocumentsParser.GetConrollerElements(webApiXmlPath);
            var modelElements = XmlDocumentsParser.GetModelElements(libXmlPath);

            var postedModels = new List<ModelDtoPage>();
            //foreach (var model in modelElements)
            var model = modelElements.FirstOrDefault(m => m.Key.Attribute("name").Value.Contains("QueueItemGroupingFilter"));
            {

                var parsedModel = ParseModel(model);
                
                var page = GetPageObject(parsedModel);

                var postedPage = PostPage(page).Result;
                parsedModel.Id = postedPage.Id;
                postedModels.Add(parsedModel);
            }

            Console.WriteLine();
            Console.WriteLine("End of program");
            Console.ReadLine();
        }

        private static ModelDtoPage ParseModel(KeyValuePair<XElement, List<XElement>> model)
        {
            var result = new ModelDtoPage();
            result.FullName = model.Key.Attribute("name").Value;
            result.Name = result.FullName.Substring(result.FullName.LastIndexOf('.') + 1);
            var properties = new List<PropertyDto>();

            foreach (var property in model.Value)
            {
                var propertyDto = new PropertyDto();

                propertyDto.FullName = property.Attribute("name").Value;
                propertyDto.Name = propertyDto.FullName.Substring(propertyDto.FullName.LastIndexOf('.') + 1);
                var innerTags = property.Elements().ToArray()[0].Elements().ToArray();
                propertyDto.CrefType = "";
                bool nullable = false;
                if (innerTags.Count() > 0)
                {
                    var seeTag = innerTags[0];
                    propertyDto.CrefType = seeTag.Attribute("cref").Value.Substring(2);
                    var nullableAttributeValue = (string)seeTag.Attribute("nullable");
                    if (nullableAttributeValue != null)
                    {
                        if (!Boolean.TryParse(nullableAttributeValue, out nullable))
                        {
                            nullable = false;
                        }
                    }
                }
                propertyDto.Nullable = nullable;

                propertyDto.Summary = ((string)property.Elements().ToArray()[0]).RemoveLineEndingsAndMultySpaces();

                properties.Add(propertyDto);
            }

            result.Properties = properties;
            return result;
        }

        private static void PostPageExample()
        {
            Page page = GetDefaultPageObject();
            page.Title = "test №29";
            page.Content = "<table><tr><td>1</td><td>2</td></tr></table>";

            var postedPage = PostPage(page).Result;
            Console.WriteLine(postedPage.Id);
        }

        private static void InitClient()
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");
            client.DefaultRequestHeaders.Add("authorization", "Basic QWxleGV5LkJ1bG9pY2h5azpJbW1vcnRhbDNA");
        }

        private static Page GetDefaultPageObject()
        {
            return new Page()
            {
                Space = new Space() { Key = spaceKey },
                Title = "",
                PageBody = new PageBody()
                {
                    BodyStorage = new BodyStorage()
                    {
                        Value = ""
                    }
                }
            };
        }
        private static Page GetPageObject(ModelDtoPage model)
        {
            var result = new Page()
            {
                Space = new Space() { Key = spaceKey },
                Title = "Parameter " + model.Name,
                PageBody = new PageBody()
                {
                    BodyStorage = new BodyStorage()
                    {
                        Value = ""
                    }
                }
                
            };

            var modelHeaders = "<tr><th>Field Name</th><th>Type</th><th>Nullable</th><th>Summary</th></tr>";

            var sb = new StringBuilder()
                    .AppendFormat("<h1>{0}</h1>", model.Name)
                    .Append("<table>")
                    .Append(modelHeaders);

            foreach (var property in model.Properties)
            {
                sb.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>",
                    property.Name,
                    property.CrefType,
                    property.Nullable,
                    property.Summary);
            }

            sb.Append("</table>");

            result.Content = sb.ToString();

            return result;
        }

        private static async Task<Page> PostPage(Page page)
        {
            var json = SerializePage(page);
            var httpContent = new StringContent(json, encoding, "application/json");
            var streamPage = client.PostAsync("https://wiki.effective-soft.com/rest/api/content", httpContent);
            var streamRespContent = (await streamPage).Content.ReadAsStreamAsync().Result;
            return DeserializePage(streamRespContent);
        }

        private static async Task<Page> PutPage(Page page)
        {
            var json = SerializePage(page);
            var httpContent = new StringContent(json, encoding, "application/json");
            var streamPage = client.PutAsync("https://wiki.effective-soft.com/rest/api/content", httpContent);
            var streamRespContent = (await streamPage).Content.ReadAsStreamAsync().Result;
            return DeserializePage(streamRespContent);
        }

        private static Page DeserializePage(Stream streamRespContent)
        {
            var pageSerializer = new DataContractJsonSerializer(typeof(Page));
            var result = pageSerializer.ReadObject(streamRespContent);
            return result as Page;
        }

        private static string SerializePage(Page page)
        {
            var pageSerializer = new DataContractJsonSerializer(typeof(Page));
            using (var memoryStream = new MemoryStream())
            using (var writer = new StreamWriter(memoryStream, encoding))
            {
                pageSerializer.WriteObject(memoryStream, page);
                return encoding.GetString(memoryStream.ToArray());
            }
        }
    }
}
