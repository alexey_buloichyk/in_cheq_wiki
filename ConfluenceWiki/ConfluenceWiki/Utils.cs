﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfluenceWiki
{
    static class Utils
    {
        public static string RemoveLineEndingsAndMultySpaces(this string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return value;
            }
            string lineSeparator = ((char)0x2028).ToString();
            string paragraphSeparator = ((char)0x2029).ToString();

            var withoutBreaks = value.Replace("\r\n", string.Empty)
                        .Replace("\n", string.Empty)
                        .Replace("\r", string.Empty)
                        .Replace(lineSeparator, string.Empty)
                        .Replace(paragraphSeparator, string.Empty)
                        .Trim();
            var previousLength = -1;
            while (withoutBreaks.Length != previousLength)
            {
                previousLength = withoutBreaks.Length;
                withoutBreaks = withoutBreaks.Replace("  ", " ");
            }
            return withoutBreaks;
        }
    }
}
