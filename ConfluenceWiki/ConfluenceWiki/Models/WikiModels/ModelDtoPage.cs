﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfluenceWiki.Models.WikiModels
{
    class ModelDtoPage
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public IEnumerable<PropertyDto> Properties { get; set; }
    }
}
