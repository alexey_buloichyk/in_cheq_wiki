﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConfluenceWiki.Models.WikiModels
{
    class PropertyDto
    {
        public string FullName { get; set; }
        public string Name { get; set; }
        public string CrefType { get; set; }
        public bool Nullable { get; set; }
        public string Summary { get; set; }
        
    }
}
