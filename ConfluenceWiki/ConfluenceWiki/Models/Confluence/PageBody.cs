﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ConfluenceWiki.Models.Confluence
{
    [DataContract(Name = "body")]
    public class PageBody
    {
        [DataMember(Name = "storage")]
        public BodyStorage BodyStorage { get; set; }        

    }
}
