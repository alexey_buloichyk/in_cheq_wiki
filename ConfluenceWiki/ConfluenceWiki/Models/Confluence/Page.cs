﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ConfluenceWiki.Models.Confluence
{
    [DataContract(Name = "Page")]
    public class Page
    {
        [DataMember(Name = "id", EmitDefaultValue = false, Order = 1)]
        public int Id { get; set; }

        [DataMember(Name = "type", Order = 2)]
        public string Type { get; set; } = "page";

        [DataMember(Name = "title", Order = 3)]
        public string Title { get; set; }

        [DataMember(Name = "space", Order = 4)]
        public Space Space { get; set; }

        [DataMember(Name = "body", Order = 5)]
        public PageBody PageBody { get; set; }

        [IgnoreDataMember]
        public string Content
        {
            get { return PageBody.BodyStorage.Value;  }
            set { PageBody.BodyStorage.Value = value; }
        }

    }
}
