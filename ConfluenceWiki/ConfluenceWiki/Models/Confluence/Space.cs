﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ConfluenceWiki.Models.Confluence
{
    [DataContract(Name = "space")]
    public class Space
    {
        [DataMember(Name = "id", EmitDefaultValue = false, Order = 1)]
        public string Id { get; set; }

        [DataMember(Name = "key", Order = 2)]
        public string Key { get; set; }
    }
}
