﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ConfluenceWiki.Models.Confluence
{
    [DataContract(Name = "storage")]
    public class BodyStorage
    {
        [DataMember(Name = "value", Order = 1)]
        public string Value { get; set; }

        [DataMember(Name = "representation", Order = 2)]
        public string Representation { get; set; } = "storage";
    }
}
