﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Globalization;

namespace ConfluenceWikiForms
{
    [DataContract(Name = "RemoteSpace")]
    public class RemoteSpace
    {
        [DataMember(Name = "key")]
        public string Key { get; set; }
    }
}
