﻿using Dapplo.Confluence;
using Dapplo.Confluence.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace ConfluenceWikiForms
{
    class RequestClass
    {
        private static readonly HttpClient client = new HttpClient();

        static void Main1(string[] args)
        {
            //InitializeDefaultHeaders(client);
            var a = Test().Result;
            Console.ReadLine();
        }

        private static void InitializeDefaultHeaders(HttpClient httpClient)
        {
            httpClient.DefaultRequestHeaders.Accept.Clear();
            //httpClient.DefaultRequestHeaders.Accept.Add(
            //    new MediaTypeWithQualityHeaderValue("application/json"));
            //client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic QWxleGV5LkJ1bG9pY2h5azpJbW1vcnRhbDNA");
        }

        private static async Task<bool> ProcessRepositories()
        {

            var serializer = new DataContractJsonSerializer(typeof(List<RemoteSpace>));
            var streamTask = client.GetStreamAsync("https://wiki.effective-soft.com/rest/api/space/cu610");
            var a = await streamTask;
            
            var repositories = serializer.ReadObject(a);

            return false;
        }

        private static async Task<bool> Test()
        {
            var confluenceClient = ConfluenceClient.Create(new Uri("https://wiki.effective-soft.com"));
            confluenceClient.SetBasicAuthentication("Alexey.Buloichyk", "Immortal3@");
            var query = Where.And(Where.Type.IsPage, Where.Text.Contains("Test Home"));
            var searchResult = await confluenceClient.Content.SearchAsync(query, limit: 1);
            foreach (var content in searchResult.Results)
            {
                Console.WriteLine(content.Body);
            }
            return true;
        }
    }
}
